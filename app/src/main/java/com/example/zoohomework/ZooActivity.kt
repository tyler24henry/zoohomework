package com.example.zoohomework

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.zoohomework.databinding.ActivityZooBinding

class ZooActivity : AppCompatActivity() {
    private lateinit var binding: ActivityZooBinding
    private var zooName = "Zoo name"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityZooBinding.inflate(layoutInflater)
        setContentView(R.layout.activity_zoo)

        val bundle = intent.extras
        bundle?.let { bundleData ->
            zooName = bundleData["zooNameKey"].toString()
            Toast.makeText(this@ZooActivity, "Welcome to $zooName", Toast.LENGTH_LONG).show()
        }
    }
}