package com.example.zoohomework.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.zoohomework.R
import com.example.zoohomework.databinding.FragmentDetailsBinding
import com.google.android.material.button.MaterialButton
import com.google.android.material.card.MaterialCardView

class DetailsFragment : Fragment() {

    private var _binding: FragmentDetailsBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentDetailsBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnGoToChocolate.setupNavigation(R.id.action_detailsFragment_to_detailFragment)
        val direction = DetailsFragmentDirections.actionDetailsFragmentToDetailFragment("Chocolate milk cow", "A cow that provides delicious chocoloate milk")
        findNavController().navigate(direction)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun MaterialButton.setupNavigation(@IdRes id: Int) {
        setOnClickListener { it.findNavController().navigate(id)}
    }
}



















