package com.example.zoohomework.ui

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.zoohomework.R
import com.example.zoohomework.ZooActivity
import com.example.zoohomework.ZooOnboardingActivity
import com.example.zoohomework.databinding.FragmentFinishBinding

class FinishFragment : Fragment() {

//    companion object {
//        private const val ZOO_NAME_KEY = "zooNameKey"
//    }

    private var _binding: FragmentFinishBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentFinishBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnGoToGettingStarted.setOnClickListener {
            findNavController().navigate(R.id.action_finishFragment_to_gettingStartedFragment)
        }
        binding.btnGoToZooActivity.setOnClickListener {
            val intent = Intent(activity, ZooActivity::class.java)
            intent.putExtra("zooNameKey", "Cow Zoo")
            startActivity(intent)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}