package com.example.zoohomework.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.zoohomework.R
import com.example.zoohomework.databinding.FragmentGettingStartedBinding

class GettingStartedFragment : Fragment() {

    private var _binding: FragmentGettingStartedBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentGettingStartedBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnGoToHello.setOnClickListener {
            findNavController().navigate(R.id.action_gettingStartedFragment_to_helloFragment)
        }
        binding.btnGoToFinish.setOnClickListener {
            findNavController().navigate(R.id.action_gettingStartedFragment_to_finishFragment)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}